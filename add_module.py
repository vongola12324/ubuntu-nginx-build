import re
import argparse

def main(rule, ngx_brotli, openssl):
    with open(rule, 'r') as fin:
        rules = fin.read()
    rules = re.sub(r"(CFLAGS=.*)$", r"\g<1> --add-module={ngx_brotli} --with-openssl={openssl}".format(ngx_brotli=ngx_brotli, openssl=openssl), rules, flags=re.MULTILINE)
    rules = re.sub(r'CFLAGS=""', r'CFLAGS="-Wno-missing-field-initializers"', rules, flags=re.MULTILINE)
    
    with open(rule, 'w') as fout:
        fout.write(rules)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process some path.')
    parser.add_argument('--rule', dest='rule', action='store', required=True)
    parser.add_argument('--brotli', dest='brotli', action='store', required=True)
    parser.add_argument('--openssl', dest='openssl', action='store', required=True)
    args = vars(parser.parse_args())
    main(args['rule'], args['brotli'], args['openssl'])

    